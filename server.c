#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <errno.h>
#include <unistd.h>		// close
#include <arpa/inet.h>	// address ops
#include <sys/socket.h>
#include <sys/poll.h>
#define MAX_CLIENTS 30


int main(int argc , char *argv[])
{
	int master_socket,	// Сокет сервера для приема соединений
	new_socket;		// Декскриптов нового сокета (после accept)
	const int max_clients = MAX_CLIENTS;
	const int max_connections = 3;
	int activity, i, valread;

	struct sockaddr_in address;	// Адрес сокета
	int addrlen;
	const int addrport = 8888;

	struct pollfd fds[MAX_CLIENTS];	// Массив для poll

	char buffer[1025];	// Буфер данных

	// Инициализируем массив для poll
	memset(fds, 0, sizeof(struct pollfd) * max_clients);

	// Создание сокета-слушателя
	if ((master_socket = socket(AF_INET , SOCK_STREAM , 0)) == 0) {
		perror("socket failed");
		exit(EXIT_FAILURE);
	}

	// Разрешение сокету повторное использование локального адреса (man 7 socket)
	int optval = 1;
	if (setsockopt(master_socket, SOL_SOCKET, SO_REUSEADDR, &optval, sizeof(optval)) < 0) {
		perror("setsockopt");
		exit(EXIT_FAILURE);
	}

	// Задание тип сокета и интерфейс:порт для прослушивания
	memset(&address, 0, sizeof(address));
	address.sin_family = AF_INET;
	address.sin_addr.s_addr = INADDR_ANY;
	address.sin_port = htons(addrport);
	addrlen = sizeof(address);

	// Связывание (bind) сокета с localhost:port
	if (bind(master_socket, (struct sockaddr *)&address, sizeof(address))<0) {
		perror("bind failed");
		exit(EXIT_FAILURE);
	}
	printf("Listener (%d) on port %d \n", master_socket, addrport);

	// Активация прослушивания
	if (listen(master_socket, max_connections) < 0) {
		perror("listen");
		exit(EXIT_FAILURE);
	}

	// Отслеживание новых подключений
	fds[0].fd = master_socket;
	fds[0].events = POLLIN;

	printf("Waiting for connections...\n");

	for(;;) {
		// Ожидание клиентов или данных (-1 - бесконечно)
		activity = poll(fds, max_clients, -1);
		if ((activity < 0) && (errno!=EINTR)) {
			printf("poll error");
			exit(EXIT_FAILURE);
		}

		// Событие для слушателя - новое входящее подключение
		if (fds[0].revents & POLLIN) {
			new_socket = accept(master_socket,
				(struct sockaddr *)&address,
				(socklen_t*)&addrlen);

			if (new_socket < 0) {
				perror("accept");
				exit(EXIT_FAILURE);
			}

			printf("New connection, socket fd is %d, address is %s:%d\n",
				new_socket, inet_ntoa(address.sin_addr),
				ntohs(address.sin_port));

			// Добавляем клиенту к списку отслеживаемых
			for (i = 1; i < max_clients; i++) {
				if (fds[i].fd == 0) {
					printf("Adding to list of sockets as %d\n" , i);
					fds[i].fd = new_socket;
					fds[i].events = POLLIN;
					break;
				}
			}
		}

		// Событие для клиентов
		for (i = 1; i < max_clients; i++) {
			if (fds[i].revents & POLLIN) {
				// Если произошло событие, но данных нет - клиент отключился
				if ((valread = read(fds[i].fd, buffer, 1024)) == 0) {
					getpeername(fds[i].fd,
						(struct sockaddr*)&address,
						(socklen_t*)&addrlen);
					printf("Host disconnected, ip %s, port %d\n",
					inet_ntoa(address.sin_addr),
					ntohs(address.sin_port));

					close( fds[i].fd );
					fds[i].fd = 0;
					fds[i].events = 0;
				}

				// Иначе - отправляем клиенту его же сообщение
				else {
					buffer[valread] = '\0';
					send(fds[i].fd, buffer, strlen(buffer)+1, 0);
					printf("Received message from %d: %s\n", fds[i].fd, buffer);
				}
			}
		}
	}

	return 0;
}

