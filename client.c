#include <stdio.h> //printf
#include <stdlib.h> //exit
#include <string.h>    //strlen
#include <sys/socket.h>    //socket
#include <arpa/inet.h> //inet_addr
#include <unistd.h> // close

int main(int argc , char *argv[])
{
	int sock;
	struct sockaddr_in server;
	char message[1000] , server_reply[2000];

	// Создание сокета
	sock = socket(AF_INET , SOCK_STREAM , 0);
	if (sock == -1) {
		perror("socket failed");
		exit(EXIT_FAILURE);
	}
	printf("Socket created\n");

	server.sin_addr.s_addr = inet_addr("127.0.0.1");
	server.sin_family = AF_INET;
	server.sin_port = htons( 8888 );

	// Соединение с сервером
	if (connect(sock, (struct sockaddr *)&server, sizeof(server)) < 0) {
		perror("connect failed");
		exit(EXIT_FAILURE);
	}

	printf("Connected\n");

	// Цикл взаимодействия с сервером
	for(;;) {
		printf("Enter message: ");
		scanf("%s" , message);

		if (send(sock, message, strlen(message), 0) < 0) {
			perror("send failed");
			exit(EXIT_FAILURE);
		}

		if (recv(sock, server_reply, 2000, 0) < 0) {
			perror("recv failed");
			exit(EXIT_FAILURE);
		}

		printf("Server reply: %s\n", server_reply);
	}

	close(sock);
	return 0;
}
