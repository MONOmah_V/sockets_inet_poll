CC=gcc
CFLAGS=-Wall

CLIENT=client.c
SERVER=server.c

all: client server

client: $(CLIENT)
	$(CC) $(CFLAGS) $(CLIENT) -o client

server: $(SERVER)
	$(CC) $(CFLAGS) $(SERVER) -o server

clean:
	rm -f client server
